public class Airfryer{
  public String brand;
  public double changeInTemperature; 
  public int temperature; 
  public int time; 
  
  public String displayTamperatureAndTime(){
   String userAttention = "Your air fryer is set on " + temperature + "degrees for " +time + "minutes. ";
   return userAttention;
  }
  public String changeTemperature(){
    
    double newTemperature = temperature+changeInTemperature;
    String newTemperatureMessage = "The new temperature is " + newTemperature;
   
    
    return newTemperatureMessage;
  }
}
  